package Grafo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Grafo {

    private ArrayList<Vertice> vertices;
    private ArrayList<Aresta> listAresta;
    private int idVerticeInicial;
    private int idVerticeFinal;
    private int numVertices;
    private int numArestas;
    private boolean dirigido;
    private boolean conexo;
    private int[][] matrizAdjacencia;   
    private final int infinito = Integer.MAX_VALUE;

    public int getInfinito() {
        return infinito;
    }
    
    public void setVertices(ArrayList<Vertice> vertices) {
        this.vertices = vertices;
    }

    public Grafo() {
        this.vertices = new ArrayList();
        this.listAresta = new ArrayList();
        this.numVertices = 0;
        this.numArestas = 0;
        this.idVerticeInicial = -1;
        this.idVerticeFinal = -1;
        this.dirigido = false;
        this.conexo = true;
    }

    public ArrayList<Aresta> getListAresta() {
        return listAresta;
    }

    public boolean isConexo() {
        return conexo;
    }

    public void setConexo(boolean conexo) {
        this.conexo = conexo;
    }

    public boolean isDirigido() {
        return dirigido;
    }

    public void setDirigido(boolean dirigido) {
        this.dirigido = dirigido;
    }
    
    public int[][] getMatrizAdjacencia() {
        return matrizAdjacencia;
    }

    public void setMatrizAdjacencia(int[][] matrizAdjacencia) {
        this.matrizAdjacencia = matrizAdjacencia;
    }
    
    public void criaMatrizAdjacencia(){
        this.matrizAdjacencia = new int [this.numVertices][this.numVertices];
        for (int l = 0; l < this.numVertices; l++) {
            for (int c = 0; c < this.numVertices; c++) {
                if(l == c){
                    this.matrizAdjacencia[l][c] = 0;
                }else{
                    this.matrizAdjacencia[l][c] = this.infinito;
                }
            }
        }
    };

    public void addAresta(int idVertice1, int idVertice2, int peso) {
        numArestas++;
        Vertice v1 = buscaVertice(idVertice1);
        Vertice v2 = buscaVertice(idVertice2);
        this.matrizAdjacencia[idVertice1][idVertice2] = peso;
        Aresta aresta = new Aresta(numArestas, v1, v2, peso);
        if (v1 != null) {
            v1.addListAdj(aresta);
            v1.addGrau();
        }
        if (!this.dirigido) {
            Aresta aresta2 = new Aresta(numArestas, v2, v1, peso);
            this.matrizAdjacencia[idVertice2][idVertice1] = peso;
            if (v2 != null) {
                v2.addListAdj(aresta2);
                v2.addGrau();
            }
        }
    }

    public void addVertice(int posX, int posY, String rotulo, int id) {
        Vertice vertice = new Vertice(id, rotulo, posX, posY);
        this.vertices.add(vertice);
        this.numVertices++;
        this.idVerticeFinal = vertice.getId();
        if (this.idVerticeInicial == -1) {
            this.idVerticeInicial = vertice.getId();
        }
    }

    public ArrayList<Vertice> getVertices() {
        return vertices;
    }

    public int getIdVerticeInicial() {
        return idVerticeInicial;
    }

    public int getIdVerticeFinal() {
        return idVerticeFinal;
    }

    public int getNumVertices() {
        return numVertices;
    }

    public Vertice buscaVertice(int idVertice) {
        Vertice v = null;
        for (int i = 0; i < this.vertices.size(); i++) {
            if (this.vertices.get(i).getId() == idVertice) {
                v = this.vertices.get(i);
                i = this.vertices.size() + 1;
            }
        }
        return v;
    }

    public Vertice searchNextVertice() {
        Vertice retorno = null;
        for (int i = 0; i < this.vertices.size(); i++) {
            if (!this.vertices.get(i).isVisitado()) {
                retorno = this.vertices.get(i);
                i = this.vertices.size();
            }
        }
        return retorno;
    }

    public void ordernaPorGrau() {
        for (int i = this.vertices.size(); i > 1; i--) {
            for (int j = 1; j < i; j++) {
                if (this.vertices.get(j - 1).getGrau() < this.vertices.get(j).getGrau()) {
                    Collections.swap(this.vertices, j, j - 1);
                }
            }
        }
    }

    public void ordernaPorVertice() {
        for (int i = this.vertices.size(); i > 1; i--) {
            for (int j = 1; j < i; j++) {
                if (this.vertices.get(j - 1).getId() > this.vertices.get(j).getId()) {
                    Collections.swap(this.vertices, j, j - 1);
                }
            }
        }
    }
    
    private boolean VerificacaoDeVizinhos(List<Vertice> vizinhosAtual) {
        Vertice atual, vizinho;
        List<Vertice> vizinhosDoVizinho = new ArrayList<>();
        for (int i = 0; i < vizinhosAtual.size(); i++) {
            atual = vizinhosAtual.get(i);
            for (int j = 0; j < vertices.size(); j++) {
                if (i==j) continue;
                vizinho = vertices.get(j);
                //if (!"".equals(matrizAdjacencia[atual.getIdMatriz()][vizinho.getIdMatriz()])) {
                
                    vizinhosDoVizinho.add(vizinho);
                
            }
            for (int j = 0; j < vizinhosAtual.size(); j++) {
                if (vizinhosDoVizinho.contains(vizinhosAtual.get(j))) return true;
            }
            vizinhosDoVizinho.clear();
        }
        return false;
    }
    
    public boolean isPlanar () {
        int aux = 0;
        
        if (verificaComprimentoCiclos()) {
            aux = (3 * numVertices) - 6;    
            
            if (numArestas <= aux){
                return true;
            }
        }else {
            aux = (2 * numVertices) - 4;
            
            if (numArestas <= aux){
                return true;
            }
        }    
        return false;
    }
    
    private boolean verificaComprimentoCiclos() {
        Vertice atual, vizinho;
        List<Vertice> vizinhosAtual = new ArrayList<>();
        for (int i = 0; i < vertices.size(); i++) {
            atual = vertices.get(i);
            for (int j = 0; j < this.vertices.size(); j++) {
                if (i==j) continue;
                vizinho = this.vertices.get(j);
                
                    vizinhosAtual.add(vizinho);
                
            }
            if (VerificacaoDeVizinhos(vizinhosAtual)) return true;
            vizinhosAtual.clear();
        }
        return false;
    }

    public int procurarRegioes () {
        int regioes = 0;
        
        if (isPlanar()) {
            regioes = 2 - numVertices + numArestas;
            return regioes;
        }
        return regioes;
    }

}
